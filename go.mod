module TaskTracker

go 1.21.1

require (
	github.com/andygrunwald/go-jira v1.16.0 // indirect
	github.com/andygrunwald/go-jira/v2 v2.0.0-20240116150243-50d59fe116d6 // indirect
	github.com/fatih/structs v1.1.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/joho/godotenv v1.5.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/trivago/tgo v1.0.7 // indirect
)
