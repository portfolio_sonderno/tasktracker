## Objectives of this project

- See all your tasks
- Add additional information to the task (comments)
- Move tasks by status
- **Only view** other person's tasks
- View tasks with an early deadline. DueDate is selected as the deadline field
- Everything should be displayed in the GUI

