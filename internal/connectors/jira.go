package connectors

import (
	"fmt"
	"github.com/joho/godotenv"
	"os"
)

type JiraAuth struct {
	Username string
	APIToken string
}

func (ja *JiraAuth) Credentials() {
	if envErr := godotenv.Load(".env"); envErr != nil {
		fmt.Println(".env file missing")
	}
	ja.Username = os.Getenv("JIRA_USER")
	ja.APIToken = os.Getenv("JIRA_TOKEN")
}

func GetAuthInfo() JiraAuth {
	ja := JiraAuth{}
	ja.Credentials()
	return ja
}
