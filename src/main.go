package main

import (
	"TaskTracker/internal/connectors"
	"context"
	"fmt"
	jira "github.com/andygrunwald/go-jira/v2/cloud"
)

func main() {
	ja := connectors.GetAuthInfo()
	tp := jira.BasicAuthTransport{
		Username: ja.Username,
		APIToken: ja.APIToken,
	}
	client, err := jira.NewClient("https://sonderno.atlassian.net/", tp.Client())
	if err != nil {
		fmt.Printf("Error when we create the client: %s", err)
	}
	//u, _, err := client.User.GetCurrentUser(context.Background())
	jql := "assignee = currentuser()"
	issues, _, err := client.Issue.Search(context.Background(), jql, nil)
	for _, issue := range issues {
		fmt.Printf("Key: %s, Summary: %s\n", issue.Key, issue.Fields.Summary)
	}
}
